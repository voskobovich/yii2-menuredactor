<?php
/**
 * Created by PhpStorm.
 * User: Vitaly Voskobovich
 * Date: 16.05.14 14:41
 */
namespace voskobovich\menuredactor;

use yii\web\AssetBundle;

class MenuRedactorAsset extends AssetBundle
{
    public $sourcePath = '@vendor/voskobovich/yii2-menuredactor/assets';
    public $js = [
        'jquery.nestable.js'
    ];
    public $css = [
        'jquery.nestable.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
